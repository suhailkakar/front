// console tab type.
export type BoostConsoleLocationTab = 'Feed' | 'Sidebar';

export type BoostConsoleStateFilterValue =
  | 'All'
  | 'Pending'
  | 'Rejected'
  | 'Approved'
  | 'Completed';
