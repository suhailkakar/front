// console tab type.
export type SupermindConsoleTab = 'Inbound' | 'Outbound';

// console subpage.
export type SupermindConsoleSubPage = 'inbox' | 'outbox';
