import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  HostBinding,
  Input,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FeedsService } from '../../../../common/services/feeds.service';
import { Session } from '../../../../services/session';
import { SortedService } from './sorted.service';
import { Client } from '../../../../services/api/client';
import { GroupsService } from '../../groups.service';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { ComposerComponent } from '../../../composer/composer.component';
import { AsyncPipe } from '@angular/common';
import { GroupsSearchService } from './search.service';
import { ToasterService } from '../../../../common/services/toaster.service';
import { FeedsUpdateService } from '../../../../common/services/feeds-update.service';

/**
 * Container for group feeds. Includes content type filter, search results,
 * and a feed of applicable activities.
 */
@Component({
  selector: 'm-group-profile-feed__sorted',
  providers: [SortedService, FeedsService],
  templateUrl: 'sorted.component.html',
  styleUrls: ['sorted.component.ng.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GroupProfileFeedSortedComponent implements OnInit, OnDestroy {
  group: any;

  // Whether this is displayed in modern groups
  @Input('v2')
  @HostBinding('class.m-group-profile-feed__sorted--v2')
  v2: boolean = false;

  @Input('group') set _group(group: any) {
    if (group === this.group) {
      return;
    }

    this.group = group;

    if (this.initialized) {
      this.load(true);
    }
  }

  type: string = 'activities';

  @Input('type') set _type(type: string) {
    if (type === this.type) {
      return;
    }

    this.type = type;

    if (this.initialized) {
      this.load(true);
    }
  }

  pinned: any[] = [];

  inProgress: boolean = false;
  moreData: boolean = true;
  offset: any = '';

  initialized: boolean = false;

  kicking: any;

  viewScheduled: boolean = false;

  @ViewChild('composer') private composer: ComposerComponent;

  scheduledCount: number = 0;

  feed$: Observable<BehaviorSubject<Object>[]>;

  groupsSearchQuerySubscription: Subscription;

  /** Listening for new posts. */
  private feedsUpdatedSubscription: Subscription;

  query: string = '';

  constructor(
    protected service: GroupsService,
    public feedsService: FeedsService,
    protected sortedService: SortedService,
    protected session: Session,
    protected router: Router,
    protected route: ActivatedRoute,
    protected client: Client,
    protected cd: ChangeDetectorRef,
    public groupsSearch: GroupsSearchService,
    public feedsUpdate: FeedsUpdateService,
    private toast: ToasterService
  ) {}

  ngOnInit() {
    this.initialized = true;

    this.groupsSearchQuerySubscription = this.groupsSearch.query$.subscribe(
      query => {
        this.query = query;
        this.load(true);
      }
    );

    this.feedsUpdatedSubscription = this.feedsUpdate.postEmitter.subscribe(
      newPost => {
        this.prepend(newPost);
      }
    );
  }

  ngOnDestroy(): void {
    if (this.groupsSearchQuerySubscription) {
      this.groupsSearchQuerySubscription.unsubscribe();
    }
    this.feedsUpdatedSubscription?.unsubscribe();
  }

  async load(refresh: boolean = false) {
    if (!refresh) {
      return;
    }

    if (refresh) {
      this.feedsService.clear();
    }

    this.detectChanges();

    let endpoint = 'api/v2/feeds/container';
    if (this.viewScheduled) {
      endpoint = 'api/v2/feeds/scheduled';
    }

    try {
      if (this.query) {
        this.viewScheduled = false;
        endpoint = 'api/v2/feeds/container';

        const params: any = {
          period: '1y',
          all: 1,
          query: this.query,
          sync: 1,
          force_public: 1,
        };
        this.feedsService.setParams(params);
      } else {
        this.feedsService.setParams({ query: '' });
      }

      this.feedsService
        .setEndpoint(`${endpoint}/${this.group.guid}/${this.type}`)
        .setLimit(12)
        .fetch();

      this.feed$ = this.feedsService.feed;

      this.getScheduledCount();
    } catch (e) {
      console.error('GroupProfileFeedSortedComponent.loadFeed', e);
    }

    this.inProgress = false;
    this.detectChanges();
  }

  loadMore() {
    if (
      this.feedsService.canFetchMore &&
      !this.feedsService.inProgress.getValue() &&
      this.feedsService.offset.getValue()
    ) {
      this.feedsService.fetch(); // load the next 150 in the background
    }
    this.feedsService.loadMore();
  }

  setFilter(type: string) {
    if (!this.v2) {
      const route = ['/groups/profile', this.group.guid, 'feed'];

      if (type !== 'activities') {
        route.push(type);
      }

      this.router.navigate(route);
    } else {
      this.router.navigate([], {
        relativeTo: this.route,
        queryParams: {
          filter: type,
        },
        queryParamsHandling: 'merge',
      });
    }
  }

  isMember() {
    return this.session.isLoggedIn() && this.group['is:member'];
  }

  isActivityFeed() {
    return this.type === 'activities';
  }

  /**
   * Prepend an activity to the feed.
   * @param { any } activity - activity to prepend.
   * @returns { void }
   */
  public prepend(activity: any): void {
    // if new activity does not belong to this group, do not prepend.
    if (
      !activity?.container_guid ||
      activity.container_guid !== this.group.guid
    ) {
      return;
    }

    if (!activity || !this.isActivityFeed()) {
      return;
    }

    if (
      this.group.moderated &&
      !(this.group['is:moderator'] || this.group['is:owner'])
    ) {
      this.toast.success(
        'Your post is pending approval from the group moderators'
      );
    }

    let feedItem = {
      entity: activity,
      urn: activity.urn,
      guid: activity.guid,
    };

    this.feedsService.rawFeed.next([
      ...[feedItem],
      ...this.feedsService.rawFeed.getValue(),
    ]);

    this.detectChanges();
  }

  delete(activity) {
    this.feedsService.deleteItem(activity, (item, obj) => {
      return item.guid === obj.guid;
    });

    this.detectChanges();
  }

  //

  canDeactivate(): boolean | Promise<boolean> {
    if (this.composer) {
      return this.composer.canDeactivate();
    }
    return true;
  }

  /**
   * Sets the value of kick, triggering the kick modal.
   *
   * @param Observable entity - entity triggering the removal
   * @returns void
   */
  kick(entity: Observable<Object>): void {
    // programmatic piping as cannot use pipes in the click event.
    if (!entity) {
      this.kicking = null;
      return;
    }
    const asyncPipe: AsyncPipe = new AsyncPipe(this.cd);
    const userValue: Object = asyncPipe.transform(entity);
    this.kicking = userValue['ownerObj'];
  }

  //

  detectChanges() {
    this.cd.markForCheck();
    this.cd.detectChanges();
  }

  toggleScheduled() {
    this.viewScheduled = !this.viewScheduled;
    this.load(true);
  }

  async getScheduledCount() {
    const url = `api/v2/feeds/scheduled/${this.group.guid}/count`;
    const response: any = await this.client.get(url);
    this.scheduledCount = response.count;
    this.detectChanges();
  }

  /**
   * Applies dontPin value to the entities
   * @param entity
   */
  patchEntity(entity) {
    entity.dontPin = !(this.group['is:moderator'] || this.group['is:owner']);
    return entity;
  }

  /**
   * Reroute to the correct endpoint for the version of groups we're using
   */
  onReviewNoticeClick($event): void {
    if (this.v2) {
      this.router.navigate(['group', this.group.guid, 'review']);
    } else {
      this.router.navigate(['groups', this.group.guid, 'feed', 'review']);
    }
  }

  /**
   * Whether a boost should be shown in a given feed position.
   * @param { number } position - index / position in feed.
   * @returns { boolean } - true if a boost should be shown in given feed position
   */
  public shouldShowBoostInPosition(position: number): boolean {
    return (
      // Displays in the 2nd slot and then every 6 posts
      (position > 4 && position % 5 === 0) || position === 0
    );
  }
}
